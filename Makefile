KEY_SIZE ?= 8192
ISO ?= trisquel-netinst_7.0_i686.iso
PASSWORD ?= pass:changeme

all: signed/$(ISO).sig

dirs:
	mkdir -p ca certs

ca/ca.key: dirs
	openssl req -batch -x509 -newkey rsa:$(KEY_SIZE) -out signed/ca.crt \
                -keyout ca/ca.key -days 30 -passin $(PASSWORD) -passout $(PASSWORD)

certs/codesign.req: ca/ca.key
	openssl req -batch -newkey rsa:$(KEY_SIZE) -keyout certs/codesign.key \
                -out certs/codesign.req -passin $(PASSWORD) -passout $(PASSWORD)


certs/codesign.crt: certs/codesign.req
	echo "01" > ca/ca.srl
	touch ca/ca.idx
	openssl ca -batch -config ca.cnf -extensions codesigning -in certs/codesign.req \
                -out certs/codesign.crt -passin $(PASSWORD)

signed/$(ISO).sig: certs/codesign.crt
	openssl cms -sign -binary -noattr -in signed/$(ISO) \
              -signer certs/codesign.crt -inkey certs/codesign.key -certfile signed/ca.crt \
              -outform DER -out signed/$(ISO).sig -passin $(PASSWORD)

clean:
	rm -f signed/$(ISO).sig signed/ca.crt
	rm -rf ca/ certs/
